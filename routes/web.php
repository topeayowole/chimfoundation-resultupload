<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Artisan Commands
Route::get('/create-symlink', function () {
    Artisan::call('storage:link');
});

Route::get('/migratefresh', function () {
    Artisan::call('migrate:fresh');
});

Route::get('/seeder', function(){
    Artisan::call('db:seed');
});


Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'ResultController@create')->name('results.create');
Route::post('/result', 'ResultController@store')->name('results.store');

Auth::routes(['register'=>false]); //Disable register route


Route::get('admin/dashboard', 'AdminDashboardController@showDashboard')->name('admin.dashboard');
Route::get('admin/results', 'AdminDashboardController@showResults')->name('admin.results');
// Route::get('admin/results/{result}', 'AdminDashboardController@showResultDetail')->name('admin.result.detail');
Route::delete('admin/results/{result}', 'AdminDashboardController@destroyResult')->name('admin.result.destroy');
Route::get('admin/excel', 'AdminDashboardController@showExcel')->name('admin.excel');
Route::get('admin/exportexcel', 'AdminDashboardController@exportExcel')->name('admin.exportexcel');

Route::get('/home', 'HomeController@index')->name('home');
