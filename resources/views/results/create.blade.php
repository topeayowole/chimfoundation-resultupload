@extends('layouts.frontend')
@section('content')
    <div id="main" class="container" style="margin-top: 50px; margin-bottom: 50px;">
        @include('partials.errorbag')
        @include('partials.successmsg')
        <form action="{{ route('results.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">First Name <span class="text-danger">*</span><span style="font-size: 10px;font-style: italic;">(Entry is required)</span></label>
                        <input type="text" name="firstname" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Last Name <span class="text-danger">*</span><span style="font-size: 10px;font-style: italic;">(Entry is required)</label>
                        <input type="text" name="lastname" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Email Address <span class="text-danger">*</span><span style="font-size: 10px;font-style: italic;">(Entry is required)</label></label>
                        <input type="text" name="email" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Date of Birth <span class="text-danger">*</span><span style="font-size: 10px;font-style: italic;">(Entry is required)</label>
                        <div class="row">
                            <div class="col-md-2">
                                <label for="">Day</label>
                                <select name="day" class="form-control">
                                    @for ($i = 1; $i <= 31; $i++)
                                        <option value="{{$i}}">{{$i}}</option>    
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="">Month</label>
                                <select name="month" class="form-control">
                                    @for ($i = 1; $i <= 12; $i++)
                                        <option value="{{$i}}">{{$i}}</option>    
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Year</label>
                                <select name="year" class="form-control">
                                    @for ($i = 1980; $i <= 2019; $i++)
                                        <option value="{{$i}}">{{$i}}</option>    
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Student Type <span class="text-danger">*</span><span style="font-size: 10px;font-style: italic;">(Entry is required)</label>
                        <select name="student_type" id="" class="form-control">
                            <option value="High School Student">High School Student</option>
                            <option value="University Student">University Student</option>
                        </select>
                    </div>
                    <div class="result-div">
                        <div class="form-group">
                            <label for="">Result Name <span class="text-danger">*</span><span style="font-size: 10px;font-style: italic;">(Entry is required)</span></label>
                            <input type="text" name="resultname[]" class="form-control resultname" >
                        </div>
                        <div class="form-group">
                            <label for="">Upload Result/Transcript (PDF Only) <span class="text-danger">*</span><span style="font-size: 10px;font-style: italic;">(Entry is required)</span></label>
                            <input type="file" name="result[]" class="result" >
                        </div>

                        {{-- <a href='#' class='remove-link'>Remove</a> --}}
                    </div>
                    <div class="form-group">
                        <a href="#" id="add-more"><i class="fa fa-plus"></i> Add more result</a>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        jQuery('#add-more').click(function(e){
            /* Prevents the default browser behaviour when a link with "#" is clicked.
             * It prevents the defaut browser behaviour by preventing the page from 
             * reloading or scrolling to the top.
            */ 
            e.preventDefault();
            let resultdiv = jQuery(this).parent().prev();
            let clonedDiv = resultdiv.clone();

            /* Now, if there is a remove-link in the previous element, the 
             * cloned element will now contain duplicate remove-links.
             * So we need to remove any existing remove-link before adding
             * a new one.
            */
            clonedDiv.children('.remove-link').remove();

            //Modify the cloned div by adding a remove link in it.
            clonedDiv.append("<a href='#' class='remove-link'>Remove</a>");

            //Clear all inputs.
            clonedDiv.children().children().val("");
            //If you want to clear the textbox in the cloned div alone.
            // clonedDiv.children().children('.resultname').val("");
            //If you want to clear the file input in the cloned div alone.
            // clonedDiv.children().children('.result').val("");
            
            //Insert the cloned div after the resultdiv.
            resultdiv.after(clonedDiv);
            // console.log(jQuery(this).parent().prev());
            
        });

        jQuery(document).on('click', '.remove-link', function(e){
            /* Prevents the default browser behaviour when a link with "#" is clicked.
             * It prevents the defaut browser behaviour by preventing the page from 
             * reloading or scrolling to the top.
            */
            e.preventDefault();

            //Get the result-div for this remove link.
            jQuery(this).parent().remove();
        });
    </script>
@endsection