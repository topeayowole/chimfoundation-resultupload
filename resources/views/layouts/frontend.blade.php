<!DOCTYPE html>
<html lang="en-US" class="no-js">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="https://chimfoundation.org/chim12dx/xmlrpc.php">
	<title>RESULT UPLOAD | The Vin-Rose Ezekwe Scholarship Foundation</title>

	<!-- All In One SEO Pack 3.6.2[281,374] -->
	<script type="application/ld+json" class="aioseop-schema">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://chimfoundation.org/chim12dx/#organization","url":"https://chimfoundation.org/chim12dx/","name":"The Vin-Rose Ezekwe Scholarship Foundation","sameAs":[]},{"@type":"WebSite","@id":"https://chimfoundation.org/chim12dx/#website","url":"https://chimfoundation.org/chim12dx/","name":"The Vin-Rose Ezekwe Scholarship Foundation","publisher":{"@id":"https://chimfoundation.org/chim12dx/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://chimfoundation.org/chim12dx/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://chimfoundation.org/chim12dx/elementary-six-high-school-students/#webpage","url":"https://chimfoundation.org/chim12dx/elementary-six-high-school-students/","inLanguage":"en-US","name":"ELEMENTARY SIX/HIGH SCHOOL STUDENTS","isPartOf":{"@id":"https://chimfoundation.org/chim12dx/#website"},"breadcrumb":{"@id":"https://chimfoundation.org/chim12dx/elementary-six-high-school-students/#breadcrumblist"},"datePublished":"2020-08-06T09:47:56+00:00","dateModified":"2020-08-06T10:00:23+00:00"},{"@type":"BreadcrumbList","@id":"https://chimfoundation.org/chim12dx/elementary-six-high-school-students/#breadcrumblist","itemListElement":[{"@type":"ListItem","position":1,"item":{"@type":"WebPage","@id":"https://chimfoundation.org/chim12dx/","url":"https://chimfoundation.org/chim12dx/","name":"The Vin-Rose Ezekwe Scholarship Foundation"}},{"@type":"ListItem","position":2,"item":{"@type":"WebPage","@id":"https://chimfoundation.org/chim12dx/elementary-six-high-school-students/","url":"https://chimfoundation.org/chim12dx/elementary-six-high-school-students/","name":"ELEMENTARY SIX/HIGH SCHOOL STUDENTS"}}]}]}</script>
	<link rel="canonical" href="index.html" />
	<!-- All In One SEO Pack -->



	<link rel='dns-prefetch' href='http://maxcdn.bootstrapcdn.com/' />
	<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
	<link rel='dns-prefetch' href='http://s.w.org/' />
	<link rel="alternate" type="application/rss+xml" title="The Vin-Rose Ezekwe Scholarship Foundation &raquo; Feed" href="https://chimfoundation.org/chim12dx/feed/" />
	<link rel="alternate" type="application/rss+xml" title="The Vin-Rose Ezekwe Scholarship Foundation &raquo; Comments Feed" href="https://chimfoundation.org/chim12dx/comments/feed/" />




	<!-- This site uses the Google Analytics by MonsterInsights plugin v7.12.0 - Using Analytics tracking - https://www.monsterinsights.com/ -->
	<script type="text/javascript" data-cfasync="false">
		var mi_version         = '7.12.0';
		var mi_track_user      = true;
		var mi_no_track_reason = '';
		
		var disableStr = 'ga-disable-UA-174278389-1';

		/* Function to detect opted out users */
		function __gaTrackerIsOptedOut() {
			return document.cookie.indexOf(disableStr + '=true') > -1;
		}

		/* Disable tracking if the opt-out cookie exists. */
		if ( __gaTrackerIsOptedOut() ) {
			window[disableStr] = true;
		}

		/* Opt-out function */
		function __gaTrackerOptout() {
		  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
		  window[disableStr] = true;
		}

		if ( 'undefined' === typeof gaOptout ) {
			function gaOptout() {
				__gaTrackerOptout();
			}
		}
		
		if ( mi_track_user ) {
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','../../../www.google-analytics.com/analytics.js','__gaTracker');

			__gaTracker('create', 'UA-174278389-1', 'auto');
			__gaTracker('set', 'forceSSL', true);
			__gaTracker('require', 'displayfeatures');
			__gaTracker('require', 'linkid', 'linkid.html');
			__gaTracker('send','pageview');
		} else {
			console.log( "" );
			(function() {
				/* https://developers.google.com/analytics/devguides/collection/analyticsjs/ */
				var noopfn = function() {
					return null;
				};
				var noopnullfn = function() {
					return null;
				};
				var Tracker = function() {
					return null;
				};
				var p = Tracker.prototype;
				p.get = noopfn;
				p.set = noopfn;
				p.send = noopfn;
				var __gaTracker = function() {
					var len = arguments.length;
					if ( len === 0 ) {
						return;
					}
					var f = arguments[len-1];
					if ( typeof f !== 'object' || f === null || typeof f.hitCallback !== 'function' ) {
						console.log( 'Not running function __gaTracker(' + arguments[0] + " ....) because you are not being tracked. " + mi_no_track_reason );
						return;
					}
					try {
						f.hitCallback();
					} catch (ex) {

					}
				};
				__gaTracker.create = function() {
					return new Tracker();
				};
				__gaTracker.getByName = noopnullfn;
				__gaTracker.getAll = function() {
					return [];
				};
				__gaTracker.remove = noopfn;
				window['__gaTracker'] = __gaTracker;
						})();
			}
	</script>
	<!-- / Google Analytics by MonsterInsights -->



	<script type="text/javascript">
		window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/chimfoundation.org\/chim12dx\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.2"}};
		/*! This file is auto-generated */
		!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
	</script>


	<style type="text/css">
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 .07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}
	</style>


	<link rel='stylesheet' id='wp-block-library-css'  href="{{asset('wp-includes/css/dist/block-library/style.min7661.css?ver=5.4.2')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='contact-form-7-css'  href="{{asset('wp-content/plugins/contact-form-7/includes/css/styles7752.css?ver=5.2.1')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='cff-css'  href="{{asset('wp-content/plugins/custom-facebook-feed/css/cff-style4165.css?ver=2.15.1')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='sb-font-awesome-css'  href='../../../maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min7661.css?ver=5.4.2')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='ctf_styles-css'  href="{{asset('wp-content/plugins/custom-twitter-feeds/css/ctf-styles.mincc1a.css?ver=1.5.1')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='gdlr-core-google-font-css'  href='https://fonts.googleapis.com/css?family=Poppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMerriweather%3A300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic&amp;subset=devanagari%2Clatin%2Clatin-ext%2Ccyrillic%2Ccyrillic-ext%2Cvietnamese&amp;ver=5.4.2' type='text/css' media='all' />
	<link rel='stylesheet' id='font-awesome-css'  href="{{asset('wp-content/plugins/goodlayers-core/plugins/fontawesome/font-awesome7661.css?ver=5.4.2')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='elegant-font-css'  href="{{asset('wp-content/plugins/goodlayers-core/plugins/elegant/elegant-font7661.css?ver=5.4.2')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='gdlr-core-plugin-css'  href="{{asset('wp-content/plugins/goodlayers-core/plugins/stylebfe4.css?ver=1596559387')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='gdlr-core-page-builder-css'  href="{{asset('wp-content/plugins/goodlayers-core/include/css/page-builder7661.css?ver=5.4.2')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='rs-plugin-settings-css'  href="{{asset('wp-content/plugins/revslider/public/assets/css/rs618cf.css?ver=6.2.3')}}" type='text/css' media='all' />
	<style id='rs-plugin-settings-inline-css')}}" type='text/css'>
	#rs-demo-id {}
	</style>
	<link rel='stylesheet' id='newsletter-css'  href="{{asset('wp-content/plugins/newsletter/stylecc6a.css?ver=6.8.4')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='wpgmp-frontend_css-css'  href="{{asset('wp-content/plugins/wp-google-map-plugin/assets/css/frontend7661.css?ver=5.4.2')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='attorna-style-core-css'  href="{{asset('wp-content/themes/chim/css/style-core7661.css?ver=5.4.2')}}" type='text/css' media='all' />
	<link rel='stylesheet' id='attorna-custom-style-css'  href="{{asset('wp-content/uploads/attorna-style-custom0494.css?1596559387&amp;ver=5.4.2')}}" type='text/css' media='all' />
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

	<script type='text/javascript'>
	/* <![CDATA[ */
	var monsterinsights_frontend = {"js_events_tracking":"true","download_extensions":"doc,pdf,ppt,zip,xls,docx,pptx,xlsx","inbound_paths":"[{\"path\":\"\\\/go\\\/\",\"label\":\"affiliate\"},{\"path\":\"\\\/recommend\\\/\",\"label\":\"affiliate\"}]","home_url":"https:\/\/chimfoundation.org\/chim12dx","hash_tracking":"false"};
	/* ]]> */
	</script>


	<script type='text/javascript' src="{{asset('wp-content/plugins/google-analytics-for-wordpress/assets/js/frontend.min16a5.js?ver=7.12.0')}}"></script>
	<script type='text/javascript' src="{{asset('wp-includes/js/jquery/jquery4a5f.js?ver=1.12.4-wp')}}"></script>
	<script type='text/javascript' src="{{asset('wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1')}}"></script>
	<script type='text/javascript' src="{{asset('wp-content/plugins/revslider/public/assets/js/rbtools.minf049.js?ver=6.0')}}"></script>
	<script type='text/javascript' src="{{asset('wp-content/plugins/revslider/public/assets/js/rs6.min18cf.js?ver=6.2.3')}}"></script>

	<!--[if lt IE 9]>
	<script type='text/javascript' src='https://chimfoundation.org/chim12dx/wp-content/themes/chim/js/html5.js?ver=5.4.2'></script>
	<![endif]-->


	<link rel='https://api.w.org/' href='https://chimfoundation.org/chim12dx/wp-json/' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://chimfoundation.org/chim12dx/xmlrpc.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://chimfoundation.org/chim12dx/wp-includes/wlwmanifest.xml" /> 
	<meta name="generator" content="WordPress 5.4.2" />
	<link rel='shortlink' href='https://chimfoundation.org/chim12dx/?p=7621' />
	<link rel="alternate" type="application/json+oembed" href="https://chimfoundation.org/chim12dx/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fchimfoundation.org%2Fchim12dx%2Felementary-six-high-school-students%2F" />
	<link rel="alternate" type="text/xml+oembed" href="https://chimfoundation.org/chim12dx/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fchimfoundation.org%2Fchim12dx%2Felementary-six-high-school-students%2F&amp;format=xml" />


	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta name="generator" content="Powered by Slider Revolution 6.2.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />

	<script type="text/javascript">function setREVStartSize(e){
		//window.requestAnimationFrame(function() {				 
			window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;	
			window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;	
			try {								
				var pw = document.getElementById(e.c).parentNode.offsetWidth,
					newh;
				pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
				e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
				e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
				e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
				e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
				e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
				e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
				e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
				if(e.layout==="fullscreen" || e.l==="fullscreen") 						
					newh = Math.max(e.mh,window.RSIH);					
				else{					
					e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
					for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
					e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
					e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
					for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
										
					var nl = new Array(e.rl.length),
						ix = 0,						
						sl;					
					e.tabw = e.tabhide>=pw ? 0 : e.tabw;
					e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
					e.tabh = e.tabhide>=pw ? 0 : e.tabh;
					e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
					for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
					sl = nl[0];									
					for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
					var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					
					newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
				}				
				if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));					
				document.getElementById(e.c).height = newh+"px";
				window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";				
			} catch(e){
				console.log("Failure at Presize of Slider:" + e)
			}					   
		//});
	  };
	</script>
</head>

<body class="page-template-default page page-id-7621 gdlr-core-body attorna-body attorna-body-front attorna-full  attorna-with-sticky-navigation  attorna-blockquote-style-1 gdlr-core-link-to-lightbox" data-home-url="https://chimfoundation.org/chim12dx/">

	<div class="attorna-mobile-header-wrap">
		<div class="attorna-top-bar">
			<div class="attorna-top-bar-background"></div>
			<div class="attorna-top-bar-container attorna-container">
				<div class="attorna-top-bar-container-inner clearfix">
					<div class="attorna-top-bar-right attorna-item-pdlr">
						<div class="attorna-top-bar-right-text">
							Opening Hours: <i class="icon_clock_alt" style="font-size: 15px ;color: #c08686 ;margin-right: 10px;"></i>Mon - Fri 08:00-16:00 
							<div style="margin-left: 12px; margin-right: 0px; display: inline; color : #c08686;">·
							</div>
						</div>
						<div class="attorna-top-bar-right-social">
							<a href="#" target="_blank" class="attorna-top-bar-social-icon" title="facebook">
								<i class="fa fa-facebook"></i>
							</a>
							<a href="#" target="_blank" class="attorna-top-bar-social-icon" title="linkedin">
								<i class="fa fa-linkedin"></i>
							</a>
							<a href="#" target="_blank" class="attorna-top-bar-social-icon" title="twitter">
								<i class="fa fa-twitter"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="attorna-mobile-header attorna-header-background attorna-style-slide attorna-sticky-mobile-navigation " id="attorna-mobile-header">
			<div class="attorna-mobile-header-container attorna-container clearfix">
				<div class="attorna-logo  attorna-item-pdlr">
					<div class="attorna-logo-inner">
						<a class="" href="https://chimfoundation.org/chim12dx/">
							<img src="{{asset('wp-content/uploads/2020/08/logo.jpg')}}" alt="logo" width="337" height="76" title="logo" />
						</a>
					</div>
				</div>
				<div class="attorna-mobile-menu-right">
					<div class="attorna-main-menu-search" id="attorna-mobile-top-search">
						<i class="fa fa-search"></i>
					</div>

					<div class="attorna-top-search-wrap" >
						<div class="attorna-top-search-close"></div>
						<div class="attorna-top-search-row">
							<div class="attorna-top-search-cell">
								<form role="search" method="get" class="search-form" action="https://chimfoundation.org/chim12dx/">
									<input type="text" class="search-field attorna-title-font" placeholder="Search..." value="" name="s">
									<div class="attorna-top-search-submit"><i class="fa fa-search"></i></div>
									<input type="submit" class="search-submit" value="Search">
									<div class="attorna-top-search-close"><i class="icon_close"></i></div>
								</form>
							</div>
						</div>
					</div>


					<div class="attorna-mobile-menu">
						<a class="attorna-mm-menu-button attorna-mobile-menu-button attorna-mobile-button-hamburger-with-border" href="#attorna-mobile-menu" >
							<i class="fa fa-bars"></i>
						</a>
						<div class="attorna-mm-menu-wrap attorna-navigation-font" id="attorna-mobile-menu" data-slide="right">
							<ul id="menu-main-navigation" class="m-menu">
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-7546">
									<a href="https://chimfoundation.org/chim12dx/">HOME</a>
								</li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7487">
									<a href="https://chimfoundation.org/chim12dx/about-vresf/">ABOUT VRESF</a>
								</li>
								<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-7620">
									<a href="#">REQUIREMENTS</a>
									<ul class="sub-menu">
										<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-7621 current_page_item menu-item-7623">
											<a href="https://chimfoundation.org/chim12dx/elementary-six-high-school-students/" aria-current="page">ELEMENTARY SIX/HIGH SCHOOL STUDENTS</a>
										</li>
										<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7626">
											<a href="https://chimfoundation.org/chim12dx/college-university-students/">COLLEGE/UNIVERSITY STUDENTS</a>
										</li>
									</ul>
								</li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7484">
									<a href="https://chimfoundation.org/chim12dx/how-to-apply/">HOW TO APPLY</a>
								</li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7485">
									<a href="https://chimfoundation.org/chim12dx/selection-process/">SELECTION PROCESS</a>
								</li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7486">
									<a href="https://chimfoundation.org/chim12dx/biographies/">BIOGRAPHIES</a>
									<ul class="sub-menu">
										<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7499">
											<a href="https://chimfoundation.org/chim12dx/biography-of-mr-vincent-nweke-ezekwe/">BIOGRAPHY OF MR. VINCENT NWEKE EZEKWE</a>
										</li>
										<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7500">
											<a href="https://chimfoundation.org/chim12dx/biography-of-mrs-roseline-oriaku-ezekwe/">BIOGRAPHY OF MRS. ROSELINE ORIAKU EZEKWE</a>
										</li>
										<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7501">
											<a href="https://chimfoundation.org/chim12dx/biography-of-dr-nnaemeka-ezekwe/">BIOGRAPHY OF DR. NNAEMEKA EZEKWE</a>
										</li>
									</ul>
								</li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7575">
									<a href="https://chimfoundation.org/chim12dx/contact-us-2/">CONTACT</a>
								</li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7585">
									<a href="https://chimfoundation.org/chim12dx/news/">NEWS</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="attorna-body-outer-wrapper">
		<div class="attorna-body-wrapper clearfix  attorna-with-frame">
			<div class="attorna-top-bar">
				<div class="attorna-top-bar-background"></div>
				<div class="attorna-top-bar-container attorna-container">
					<div class="attorna-top-bar-container-inner clearfix">
						<div class="attorna-top-bar-left attorna-item-pdlr">
							Scholarship Foundation in honour of the memories of Mr. Vincent  Nweke Ezekwe & Mrs. Roseline Oriaku Ezekwe
						</div>
						<a class="attorna-top-bar-right-button" href="http://chimfoundation.org/chimfoundation/public" target="_self"  >APPLY FOR SCHOLARSHIP</a>
						<div class="attorna-top-bar-right attorna-item-pdlr">
							{{-- <div class="attorna-top-bar-right-text">
								Opening Hours: <i class="icon_clock_alt" style="font-size: 15px ;color: #c08686 ;margin-right: 10px ;"></i>Mon - Fri 08:00-16:00 
								<div style="margin-left: 12px; margin-right: 0px; display: inline; color : #c08686;">·
								</div>
							</div> --}}
							<div class="attorna-top-bar-right-social">
								<a href="https://web.facebook.com/vresfonline" target="_blank" class="attorna-top-bar-social-icon" title="facebook">
									<i class="fa fa-facebook"></i>
								</a>
								<a href="https://twitter.com/vresfonline" target="_blank" class="attorna-top-bar-social-icon" title="twitter">
									<i class="fa fa-twitter"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<header class="attorna-header-wrap attorna-header-style-bar attorna-header-background  attorna-style-left">
				<div class="attorna-header-container clearfix  attorna-container">
					<div class="attorna-header-container-inner">
					<div class="attorna-logo  attorna-item-pdlr">
						<div class="attorna-logo-inner">
							<a class="" href="https://chimfoundation.org/chim12dx/">
								<img src="{{asset('wp-content/uploads/2020/08/VRESF-Official-logo.jpg')}}" alt="VRESF-Official-logo" width="355" height="72" title="VRESF-Official-logo"/>
							</a>
						</div>
					</div>
					<div class="attorna-logo-right-text attorna-item-pdlr clearfix">
						<div class="attorna-logo-right-box-wrap">
							<i class="attorna-logo-right-box-icon fa fa-phone"></i>
							<div class="attorna-logo-right-box-content-wrap">
								<div class="attorna-logo-right-box-title">Phone</div>
								<div class="attorna-logo-right-box-caption">+1-832-447-3462</div>
							</div>
						</div>
						<div class="attorna-logo-right-box-wrap">
							<i class="attorna-logo-right-box-icon fa fa-envelope"></i>
							<div class="attorna-logo-right-box-content-wrap">
								<div class="attorna-logo-right-box-title">Contact Email</div>
								<div class="attorna-logo-right-box-caption">info@chimfoundation.org</div>
							</div>
						</div>
						<div class="attorna-logo-right-box-wrap">
							<i class="attorna-logo-right-box-icon fa fa-clock-o"></i>
							<div class="attorna-logo-right-box-content-wrap">
								<div class="attorna-logo-right-box-title">Opening Hours</div>
								<div class="attorna-logo-right-box-caption">Mon - Fri 08:00-16:00</div>
							</div>
						</div>
						
					</div>
				</div>
			</header><!-- header -->

			<div class="attorna-navigation-bar-wrap attorna-navigation-header-style-bar  attorna-style-solid attorna-sticky-navigation attorna-sticky-navigation-height attorna-style-left  attorna-style-fixed">
				<div class="attorna-navigation-background"></div>
				<div class="attorna-navigation-container clearfix  attorna-container">
					<div class="attorna-navigation attorna-item-pdlr clearfix attorna-navigation-submenu-indicator">
						<div class="attorna-main-menu" id="attorna-main-menu">
							<ul id="menu-main-navigation-1" class="sf-menu">
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-7546 attorna-normal-menu">
									<a href="https://chimfoundation.org/chim12dx/">HOME</a>
								</li>
								<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7487 attorna-normal-menu">
									<a href="https://chimfoundation.org/chim12dx/about-vresf/">ABOUT VRESF</a>
								</li>
								<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-7620 attorna-normal-menu">
									<a href="#" class="sf-with-ul-pre">REQUIREMENTS</a>
									<ul class="sub-menu">
										<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-7621 current_page_item menu-item-7623" data-size="60">
											<a href="https://chimfoundation.org/chim12dx/elementary-six-high-school-students/">ELEMENTARY SIX/HIGH SCHOOL STUDENTS</a>
										</li>
										<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7626" data-size="60">
											<a href="https://chimfoundation.org/chim12dx/college-university-students/">COLLEGE/UNIVERSITY STUDENTS</a>
										</li>
									</ul>
								</li>
								<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7484 attorna-normal-menu">
									<a href="https://chimfoundation.org/chim12dx/how-to-apply/">HOW TO APPLY</a>
								</li>
								<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7485 attorna-normal-menu">
									<a href="https://chimfoundation.org/chim12dx/selection-process/">SELECTION PROCESS</a>
								</li>
								<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7486 attorna-normal-menu">
									<a href="https://chimfoundation.org/chim12dx/biographies/" class="sf-with-ul-pre">BIOGRAPHIES</a>
									<ul class="sub-menu">
										<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7499" data-size="60">
											<a href="https://chimfoundation.org/chim12dx/biography-of-mr-vincent-nweke-ezekwe/">BIOGRAPHY OF MR. VINCENT NWEKE EZEKWE</a>
										</li>
										<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7500" data-size="60">
											<a href="https://chimfoundation.org/chim12dx/biography-of-mrs-roseline-oriaku-ezekwe/">BIOGRAPHY OF MRS. ROSELINE ORIAKU EZEKWE</a>
										</li>
										<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7501" data-size="60">
											<a href="https://chimfoundation.org/chim12dx/biography-of-dr-nnaemeka-ezekwe/">BIOGRAPHY OF DR. NNAEMEKA EZEKWE</a>
										</li>
									</ul>
								</li>
								<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7575 attorna-normal-menu">
									<a href="https://chimfoundation.org/chim12dx/contact-us-2/">CONTACT</a>
								</li>
								<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7585 attorna-normal-menu">
									<a href="https://chimfoundation.org/chim12dx/news/">NEWS</a>
								</li>
							</ul>
						</div>
						<div class="attorna-main-menu-right-wrap clearfix">
							<div class="attorna-main-menu-search" id="attorna-top-search">
								<i class="fa fa-search"></i>
							</div>
							<div class="attorna-top-search-wrap">
								<div class="attorna-top-search-close"></div>
								<div class="attorna-top-search-row">
									<div class="attorna-top-search-cell">
										<form role="search" method="get" class="search-form" action="https://chimfoundation.org/chim12dx/">
											<input type="text" class="search-field attorna-title-font" placeholder="Search..." value="" name="s">
											<div class="attorna-top-search-submit">
												<i class="fa fa-search"></i>
											</div>
											<input type="submit" class="search-submit" value="Search">
											<div class="attorna-top-search-close">
												<i class="icon_close"></i>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>		
					</div><!-- attorna-navigation -->
				</div><!-- attorna-header-container -->
			</div><!-- attorna-navigation-bar-wrap -->


			<!-- Overlay Image -->
			<div class="attorna-page-title-wrap  attorna-style-custom attorna-center-align" >
				<div class="attorna-header-transparent-substitute"></div>
				<div class="attorna-page-title-overlay"></div>
				<div class="attorna-page-title-container attorna-container">
					<div class="attorna-page-title-content attorna-item-pdlr">
						<h1 class="attorna-page-title">TRANSCRIPT/RESULTS UPLOAD</h1>
					</div>
				</div>
            </div>
            
            @yield('content')

            <footer>
				<div class="attorna-footer-wrapper">
					<div class="attorna-footer-container attorna-container clearfix">
						<div class="attorna-footer-column attorna-item-pdlr attorna-column-20">
							<div id="text-1" class="widget widget_text attorna-widget">			
								<div class="textwidget">
									<p>
										<img class="alignnone size-medium wp-image-7401" src="{{asset('wp-content/uploads/2020/08/logo-300x68.jpg')}}" alt="logo" width="300" height="68" /><br />
										<span class="gdlr-core-space-shortcode" style="margin-top: -27px ;"></span><br/>
										The Vin-Rose Ezekwe Scholarship Foundation (VRESF) is a non-profit philantropic foundation
									</p>
									{{-- <div class="gdlr-core-social-network-item gdlr-core-item-pdb  gdlr-core-none-align" style="padding-bottom: 0px;">
										<a href="#url" target="_blank" class="gdlr-core-social-network-icon" title="facebook" style="font-size: 16px; color: #b1976b;" rel="noopener noreferrer">
											<i class="fa fa-facebook"></i>
										</a>
										<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="linkedin" style="font-size: 16px;color: #b1976b ;" rel="noopener noreferrer">
											<i class="fa fa-linkedin"></i>
										</a>
										<a href="#url" target="_blank" class="gdlr-core-social-network-icon" title="twitter" style="font-size: 16px;color: #b1976b;" rel="noopener noreferrer">
											<i class="fa fa-twitter"></i>
										</a>
									</div> --}}
								</div>
							</div>
						</div>
						<div class="attorna-footer-column attorna-item-pdlr attorna-column-20">
							<div id="text-7" class="widget widget_text attorna-widget">
								<h3 class="attorna-widget-title">Contact Info</h3><span class="clear"></span>			
								<div class="textwidget">
									<p>Phone number: 1-832-447-3462
									</p>
									<p>info@chimfoundation.org</p>
								</div>
							</div>
						</div>
						<div class="attorna-footer-column attorna-item-pdlr attorna-column-20">
							<div id="text-10" class="widget widget_text attorna-widget">
								<div class="textwidget">
									<p>
										<a href="#">
											<img class="alignnone wp-image-7612 size-full" src="{{asset('wp-content/uploads/2020/08/Apply-for-Scholarship.jpg')}}" alt="Apply-for-Scholarship" width="419" height="245"/>
										</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="attorna-copyright-wrapper">
					<div class="attorna-copyright-container attorna-container clearfix">
						<div class="attorna-copyright-left attorna-item-pdlr">
							Copyright 2020 Chimfoundation, All Right Reserved
						</div>
						<div class="attorna-copyright-right attorna-item-pdlr">
							<a href="https://chimfoundation.org/terms-of-use-agreement/" style="margin-left:21px;">TERMS OF USE AGREEMENT</a>
							<a href="https://chimfoundation.org/privacy-policy-2/" style="margin-left:21px;">PRIVACY POLICY</a>
							<a href="https://chimfoundation.org/contact-us-2/" style="margin-left:21px;">CONTACT</a>
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>

	<!-- Custom Facebook Feed JS -->
	<script type="text/javascript">
	var cfflinkhashtags = "true";
	</script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var wpcf7 = {"apiSettings":{"root":"https:\/\/chimfoundation.org\/chim12dx\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
	/* ]]> */
	</script>
	<script type='text/javascript' src="{{asset('wp-content/plugins/contact-form-7/includes/js/scripts7752.js?ver=5.2.1')}}"></script>
	<script type='text/javascript' src="{{asset('wp-content/plugins/custom-facebook-feed/js/cff-scripts4165.js?ver=2.15.1')}}"></script>
	<script type='text/javascript' src="{{asset('wp-content/plugins/goodlayers-core/plugins/scriptbfe4.js?ver=1596559387')}}"></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var gdlr_core_pbf = {"admin":"","video":{"width":"640","height":"360"},"ajax_url":"https:\/\/chimfoundation.org\/chim12dx\/wp-admin\/admin-ajax.php"};
	/* ]]> */
	</script>
	<script type='text/javascript' src="{{asset('wp-content/plugins/goodlayers-core/include/js/page-builderd36b.js?ver=1.3.9')}}"></script>
	<script type='text/javascript' src='https://maps.google.com/maps/api/js?libraries=geometry%2Cplaces%2Cweather%2Cpanoramio%2Cdrawing&amp;language=en&amp;ver=5.4.2'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var wpgmp_local = {"all_location":"All","show_locations":"Show Locations","sort_by":"Sort by","wpgmp_not_working":"Not working...","place_icon_url":"https:\/\/chimfoundation.org\/chim12dx\/wp-content\/plugins\/wp-google-map-plugin\/assets\/images\/icons\/"};
	/* ]]> */
	</script>
	<script type='text/javascript' src="{{asset('wp-content/plugins/wp-google-map-plugin/assets/js/maps531b.js?ver=2.3.4')}}"></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var newsletter = {"messages":{"email_error":"Email address is not correct","name_error":"Name is required","surname_error":"Last name is required","profile_error":"A mandatory field is not filled in","privacy_error":"You must accept the privacy policy"},"profile_max":"20"};
	/* ]]> */
	</script>
	<script type='text/javascript' src="{{asset('wp-content/plugins/newsletter/subscription/validatecc6a.js?ver=6.8.4')}}"></script>
	<script type='text/javascript' src="{{asset('wp-includes/js/jquery/ui/effect.mine899.js?ver=1.11.4')}}"></script>
	<script type='text/javascript' src="{{asset('wp-content/themes/chim/js/jquery.mmenu8a54.js?ver=1.0.0')}}"></script>
	<script type='text/javascript' src="{{asset('wp-content/themes/chim/js/jquery.superfish8a54.js?ver=1.0.0')}}"></script>
	<script type='text/javascript' src="{{asset('wp-content/themes/chim/js/script-core8a54.js?ver=1.0.0')}}"></script>
	<script type='text/javascript' src="{{asset('wp-includes/js/wp-embed.min7661.js?ver=5.4.2')}}"></script>
	<script src="{{asset('js/vue-production.js')}}"></script>
	<script src="{{asset('js/axios.js')}}"></script>
	@yield('scripts')
</body>
</html>