@if (count($errors) > 0)
    {{-- <style>
        .text{
            color: white; 
            display: inline-block; 
            margin-right: 10px;
        }
    </style>

    <div style="padding: 20px 30px; background: rgb(243, 156, 18) none repeat scroll 0% 0%; z-index: 999999; font-size: 16px; font-weight: 600;">
        <a class="pull-right" href="#" data-toggle="tooltip" data-placement="left" style="color: rgb(255, 255, 255); font-size: 20px;">×</a>
        @foreach ($errors->all() as $error)
            <p style="color:#fff;">{{ $error }}</p>
        @endforeach
    </div> --}}

    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Oops!</h4>
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
    
@if (session('error'))
    {{-- <style>
        .text{
            color: white; 
            display: inline-block; 
            margin-right: 10px;
        }
    </style>

    <div style="padding: 20px 30px; background: rgb(243, 156, 18) none repeat scroll 0% 0%; z-index: 999999; font-size: 16px; font-weight: 600;">
        <a class="pull-right" href="#" data-toggle="tooltip" data-placement="left" style="color: rgb(255, 255, 255); font-size: 20px;">×</a>
        <p style="color:#fff;">{{ session('error')}}</p>
    </div> --}}

    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Heads up!</h4>
        {{ session('error')}}
    </div>

@endif

