@extends('layouts.dashboard')

@section('content')
    @include('partials.successmsg')
    @include('partials.errorbag')
    <section class="content-header">
        <h1>Export to Excel</h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="box-title">Export to Excel</div>
                    </div>
                    <div class="box-body">
                        <a href="{{route('admin.exportexcel')}}" class="btn btn-info btn-flat">Export</a>
                        {{-- <a href="#" class="btn btn-info btn-flat">Export</a> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
  <!-- /.content-wrapper -->
@endsection