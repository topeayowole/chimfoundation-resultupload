@extends('layouts.dashboard')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Result Detail
      </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Result Detail </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-5">
                        <table class="table table-hover table-bordered">
                            <tr>
                                <td><label>Applicant ID</label></td>
                                <td>{{ $result->applicant->applicant_id }}</td>
                            </tr>
                            <tr>
                                <td><label>First Name</label></td>
                                <td>{{ $result->applicant->firstname }}</td>
                            </tr>
                            <tr>
                                <td><label>Last Name</label></td>
                                <td>{{ $result->applicant->lastname }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection