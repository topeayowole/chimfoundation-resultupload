@extends('layouts.dashboard')

@section('content')
    @include('partials.successmsg')
    @include('partials.errorbag')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Results
      </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid" id="app">
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Results</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Applicant ID</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Email</th>
                                <th>Date of Birth</th>
                                <th>Result Name</th>
                                <th>Date Submitted</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($results as $result)
                                <tr>
                                    <td>{{ $result->id }}</td>
                                    <td>{{ $result->applicant->applicant_id }}</td>
                                    <td>{{ $result->applicant->firstname }}</td>
                                    <td>{{ $result->applicant->lastname }}</td>
                                    <td>{{ $result->applicant->email }}</td>
                                    <td>{{ $result->applicant->date_of_birth->format('jS F, Y') }}</td>
                                    <td>{{ $result->resultname }}</td>
                                    <td>{{ $result->created_at->format('jS F, Y') }}</td>
                                    {{-- <td><a href="#" class="btn btn-sm btn-info btn-flat">View</a></td> --}}
                                    {{-- <td><a href="{{ route('admin.result.detail', ['id'=>$result->id]) }}" class="btn btn-sm btn-info btn-flat">View</a></td> --}}
                                    <td><a href="{{asset('storage/'.$result->result)}}" target="_blank" class="btn btn-sm btn-info btn-flat">View</a></td>
                                    <td>
                                        <form action="{{ route('admin.result.destroy', ['id'=>$result->id]) }}" method="post">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" title="Trash" class="btn btn-sm btn-danger btn-flat" onClick='return confirm("Are you sure you want to delete?")'><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8"><p class="text-center">No Result.</p></td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('scripts')
   <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
   <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
   <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
   <script>
       $('#example1').DataTable({
           "aaSorting": []
       });
   </script> 
@endsection