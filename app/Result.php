<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    //
    protected $fillable = [
        'applicant_id', 'resultname', 'result'
    ];

    public function applicant(){
        return $this->belongsTo('App\Applicant');
    }
}
