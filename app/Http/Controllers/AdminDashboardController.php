<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Applicant;
use App\Result;
use Illuminate\Support\Facades\Storage;
use League\Csv\Writer;
use SplTempFileObject;


class AdminDashboardController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }

    public function showDashboard(){
        $total_applicants = Applicant::count();
        $total_high_school_applicants = Applicant::where('student_type', 'High School Student')->count();
        $total_university_applicants = Applicant::where('student_type', 'University Student')->count();
        $total_results = Result::count();
        $latest_results = Result::latest()->limit(7)->get();

        return view('admin.dashboard', compact('total_applicants', 'total_high_school_applicants', 'total_university_applicants', 'total_results', 'latest_results'));
    }

    public function showResults(){
        $results = Result::latest()->get();
        return view('admin.results', compact('results'));
    }

    public function showResultDetail(Result $result){
        return view('admin.result-detail', compact('result'));
    }

    public function destroyResult(Result $result){
        //Delete the result file
        if (Storage::disk('public')->exists($result->result)) {
            Storage::disk('public')->delete($result->result);
        }

        $result->delete();

        return back()->with('success', 'Result successfully deleted');
    }

    public function showExcel(){
         return view('admin.excel');
    }

    public function exportExcel(){
        $results = Result::all();

        $data = [];

        foreach ($results as $obj) {
           $array_obj = (array) $obj; 
           array_push($data, $array_obj); 
        }

        //we create the CSV into memory
        $csv = Writer::createFromFileObject(new SplTempFileObject());

        //we insert the CSV header
        $csv->insertOne(['Applicant ID', 'Firstname', 'Lastname', 'Email', 'Date of Birth', 'Student Type', 'Result Name', 'Result', 'Date Submitted']);

        foreach ($results as $result) {
            $csv->insertOne([
                $result->applicant->applicant_id,
                $result->applicant->firstname,
                $result->applicant->lastname,
                $result->applicant->email,
                $result->applicant->date_of_birth,
                $result->applicant->student_type,
                $result->resultname,
                $result->result!=null ? url('storage/'.$result->result) : '',
                $result->created_at->format('jS F, Y'),
            ]);
        }

         return response((string) $csv, 200, [
            'Content-Type' => 'text/csv',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => 'attachment; filename="VRESF-results.csv"',
        ]);
    }
}
