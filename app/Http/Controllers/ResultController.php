<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Applicant;
use App\Result;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('results.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $this->validate(request(), [
            'firstname'=>'required|min:3',
            'lastname'=>'required|min:3',
            'email'=>'required',
            'resultname.*'=>'required',
            'result.*'=>'required|mimes:pdf',
        ],[
            'resultname.*.required'=>'Result Name is required.',
            'result.*.required'=>'Result/Transcript is required.',
            'result.*.mimes'=>'Result/Transcript must be of file type: pdf.',
        ]);

        //First of all, check if the email address of the user exists.
        $user_exists = Applicant::where('email', $request->email)->exists();


        if (!$user_exists) {
            $applicant = Applicant::create([
                'firstname'=>$request->firstname,
                'lastname'=>$request->lastname,
                'email'=>$request->email,
                'date_of_birth'=>$request->year.'-'.$request->month.'-'.$request->day,
                'student_type'=>$request->student_type,
            ]);

            //Update the applicant ID
            if ($applicant->student_type == 'High School Student') {
                $applicant->applicant_id = 'H'. (100+$applicant->id);
            }else if ($applicant->student_type == 'University Student') {
                $applicant->applicant_id = 'U'.(100+$applicant->id);
            }

            $applicant->update();

            //Update the corresponding "results" table accordingly.
            for ($i=0; $i < count($request->resultname); $i++) { 
                Result::create([
                    'applicant_id'=>$applicant->id,
                    'resultname'=>$request->resultname[$i],
                    'result'=>$request->file('result')[$i]->storeAs('results//', $applicant->firstname.'-'.$applicant->lastname.'-'.$applicant->applicant_id.'-'.$request->resultname[$i].'.'.$request->file('result')[$i]->getClientOriginalExtension(), 'public'),
                ]);
            }
        }else{
            $applicant = Applicant::where('email', $request->email)->firstOrFail();

            //Update the corresponding "results" table accordingly.
            for ($i=0; $i < count($request->resultname); $i++) { 
                Result::create([
                    'applicant_id'=>$applicant->id,
                    'resultname'=>$request->resultname[$i],
                    'result'=>$request->file('result')[$i]->storeAs('results//', $applicant->firstname.'-'.$applicant->lastname.'-'.$applicant->applicant_id.'-'.$request->resultname[$i].'.'.$request->file('result')[$i]->getClientOriginalExtension(), 'public'),
                ]);
            }
        }

        return back()->with('success', 'Result successfully submitted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
