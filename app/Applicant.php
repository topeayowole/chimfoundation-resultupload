<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    //
    protected $fillable = [
        'applicant_id', 'firstname', 'lastname', 'email', 'date_of_birth', 'student_type'
    ];

    protected $dates=['date_of_birth'];

    public function results(){
        return $this->hasMany('App\Result');
    }
}
